fn main() {
    // "combining marks" ranges from https://www.unicode.org/charts/
    // some reserved ranges are not fully filled out. The non-occupied values are not included here
    let all_diacritic_ranges = (0x300..=0x036F)
        .chain(0x1AB0..=0x1ACE)
        .chain(0x1DC0..=0x1DFF)
        .chain(0x20D0..=0x20F0)
        .chain(0xFE20..=0xFE2f);
    // hand-curated from above range
    let faulty_diacritics = [
        0x300, 0x301, 0x302, 0x303, 0x304, 0x306, 0x307, 0x308, 0x309, 0x30a, 0x30b, 0x30c, 0x30f,
        0x310, 0x311, 0x313, 0x314, 0x31b, 0x323, 0x324, 0x325, 0x326, 0x327, 0x328, 0x32d, 0x32e,
        0x330, 0x331, 0x335, 0x339, 0x340, 0x341, 0x343, 0x344,
    ];

    for diacritic_codepoint in all_diacritic_ranges {
        let diacritic: char = TryFrom::<u32>::try_from(diacritic_codepoint).unwrap();
        print!(
            "{:#06x}: http://abc_{}o.com",
            diacritic_codepoint, diacritic
        );

        if faulty_diacritics.iter().any(|i| *i == diacritic_codepoint) {
            println!(" [*]");
        } else {
            println!("");
        }
    }
}
