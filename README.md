**Bug reported as [Bug 1897646](https://bugzilla.mozilla.org/show_bug.cgi?id=1897646). Marked as known/mainly non-issue with more info in [Bug 1507582](https://bugzilla.mozilla.org/show_bug.cgi?id=1507582).** As such, I'm making my research public.

# Description
## Background
Domain names allow for non-ASCII-range, Unicode scalar values to be used. Malicious actors used to abuse this feature for phishing purposes by copying well-known, legitimate domain names and replacing a few of the characters with nigh-indistinguishable non-ASCII-range alternatives. For example, by replacing the Latin a with the Cyrillic а. This is known as the IDN homograph attack (https://en.wikipedia.org/wiki/IDN_homograph_attack).

Browsers attempt to combat this attack by converting URLs with such mixed set of graphemes. Instead of displaying the human readable Unicode domain in the URL bar, the user is shown the punycode encoded variant. The idea is that while regular users may not understand what they see, they understand that they are not on the website they expect.

# The bug
I discovered some Unicode combining mark scalar values that, when combined with an ASCII-range scalar value, will not trigger the conversion to punycode in the URL-bar. An example is http://abc_̆o.com (punycode http://xn--abc_o-4fd.com), where every scalar value is within the ASCII-range except for the one between the _ (underscore) and in front of the o (15th letter of Latin alphabet). For an (as far as I was able to find) exhaustive list of scalar values, see the PoC section.
There are 3 reasons I'm convinced this is a bug:
    - inconsistency compared to other browsers: Edge and Chrome handle the example URL properly. That is, they convert it to punycode. I'd love to compare to other engines, but alas every browser but Firefox is Chromium based these days...
    - inconsistency within Firefox: while the domain is not converted in the URL bar, it is converted in the "Server Not Found" error page shown by Firefox. The above example will return following error: "We can’t connect to the server at xn--abc_o-4fd.com.".
    - no pattern in transgressing scalar values: the scalar values triggering this bug are not a fully continuous block of code points.

## Relevance/impact
Abuse of this bug enables the use of the IDN homograph attack, albeit with a limited range of combining mark scalar values. A spear phishing campaign against victims using Firefox is possible.

# PoC
## Description
Initially, the bug was discovered by complete accident on a specific domain. For completeness sake, I wrote a few lines of Rust to test which combining mark scalar values trigger the bug. The code takes the URL template http://abc_{}o.com, consisting of only ASCII-range scalar values. It then iterates over the scalar values for Unicode combining markers (see https://www.unicode.org/charts/ > Combining Marks) and inserts them into the above URL. The combination of the ASCII-range abc and the now marked o should force punycode conversion.
Unfortunately, due to inexperience with the Firefox code-base, I was unable to automate the check whether the converion actually happens. I thus had to manually open every generated link.

## Reproduce
1. Compile & run attached main.rs (no dependencies).
2. Copy any URL with a marking ([*]) next to it.
3. Paste the URL into Firefox' URL bar and press Enter.
4. Notice how the URL is not converted to punycode, while the error does mention the punycode encoded URL.

## Transgressing scalar values
```rust
let faulty_diacritics = [
    0x300, 0x301, 0x302, 0x303, 0x304, 0x306, 0x307, 0x308, 0x309, 0x30a, 0x30b, 0x30c, 0x30f, 0x310, 0x311, 0x313, 0x314, 0x31b, 0x323, 0x324, 0x325, 0x326, 0x327, 0x328, 0x32d, 0x32e, 0x330, 0x331, 0x335, 0x339, 0x340, 0x341, 0x343, 0x344
];
```

# Note
My knowledge of Unicode terminology is introductory at best (having delved into it only after discovery of this bug). Apologies for possible confusions as a result of this.
